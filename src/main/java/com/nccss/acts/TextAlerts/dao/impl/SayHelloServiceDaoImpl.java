package com.nccss.acts.TextAlerts.dao.impl;

import java.sql.Types;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import com.nccss.acts.TextAlerts.dao.SayHelloServiceDao;
import com.nccss.acts.TextAlerts.dao.rowmap.CodeLookUpRowMapper;
import com.nccss.acts.TextAlerts.model.CodeLookUp;

@Repository
public class SayHelloServiceDaoImpl implements SayHelloServiceDao {
	
	 @Autowired
	 private Environment env;
	
	 @Autowired
	 JdbcTemplate jdbcTemplate;
	 private SimpleJdbcCall procCaseApplication1;
	 
	@Autowired
	public void setDataSource(DataSource dataSource)
		{
			this.jdbcTemplate = new JdbcTemplate(dataSource);
			this.jdbcTemplate.setSkipUndeclaredResults(true);
			procCaseApplication1 = new SimpleJdbcCall(jdbcTemplate).withoutProcedureColumnMetaDataAccess().withSchemaName(env.getProperty("application.region")).withProcedureName("FKWEB_R_CODE_LOOKUP");
		}
		
	@Override
	public List<CodeLookUp> getCodeLookup(String lookup) {
		procCaseApplication1.declareParameters(new SqlParameter("CODETYPE", Types.CHAR)).returningResultSet("lookup", new CodeLookUpRowMapper());

		SqlParameterSource in = new MapSqlParameterSource().addValue("CODETYPE", lookup);

		Map<String, Object> results = procCaseApplication1.execute(in);

		return (List<CodeLookUp>) results.get("lookup");
	}

}
