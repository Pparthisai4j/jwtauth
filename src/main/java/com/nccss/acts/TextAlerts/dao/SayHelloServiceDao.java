package com.nccss.acts.TextAlerts.dao;

import java.util.List;

import com.nccss.acts.TextAlerts.model.CodeLookUp;

public interface SayHelloServiceDao {

	
	public List<CodeLookUp> getCodeLookup(String lookup) ;
}
