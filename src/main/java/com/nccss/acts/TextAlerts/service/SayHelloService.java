package com.nccss.acts.TextAlerts.service;

import java.util.List;

import com.nccss.acts.TextAlerts.model.CodeLookUp;

public interface SayHelloService {

	public List<CodeLookUp> getCodeLookup(String lookup) ;

}
