package com.nccss.acts.TextAlerts.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nccss.acts.TextAlerts.dao.SayHelloServiceDao;
import com.nccss.acts.TextAlerts.model.CodeLookUp;
import com.nccss.acts.TextAlerts.service.SayHelloService;
@Service
public class SayHelloServiceImpl implements SayHelloService {
	
	@Autowired
	SayHelloServiceDao sayHelloServiceDao;
	@Override
	public List<CodeLookUp> getCodeLookup(String lookup) {
		// TODO Auto-generated method stub
		return sayHelloServiceDao.getCodeLookup(lookup);
	}

}
