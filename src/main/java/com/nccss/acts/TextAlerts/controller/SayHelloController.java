package com.nccss.acts.TextAlerts.controller;

import java.util.List;

 
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import com.nccss.acts.TextAlerts.model.AuthenticationRequest;
import com.nccss.acts.TextAlerts.model.AuthenticationResponse;
import com.nccss.acts.TextAlerts.model.CodeLookUp;
import com.nccss.acts.TextAlerts.service.SayHelloService;
import com.nccss.acts.TextAlerts.service.impl.UserService;
import com.nccss.acts.TextAlerts.util.JWTUtility;
 
 

@RestController
public class SayHelloController {
	 
	
	@Autowired
    private JWTUtility jwtUtility;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private UserService userService;
    
	@Autowired
	SayHelloService sayHelloService;
	
	@GetMapping (path= "/nccss/optout/{lookup}")
	public ResponseEntity<Object> getCodeDetail(@PathVariable String lookup){
		 
		List<CodeLookUp> appStatusLookup = sayHelloService.getCodeLookup(lookup);
		return new ResponseEntity<>(appStatusLookup,HttpStatus.OK);
		
	}
	
	@PostMapping (path= "/nccss/authenticate")
	public AuthenticationResponse authenticate(@RequestBody AuthenticationRequest authenticationRequest) throws Exception {
		 try {
	            authenticationManager.authenticate(
	                    new UsernamePasswordAuthenticationToken(
	                    		authenticationRequest.getUsername(),
	                    		authenticationRequest.getPassword()
	                    )
	            );
	        } catch (BadCredentialsException e) {
	            throw new Exception("INVALID_CREDENTIALS", e);
	        }

	        final UserDetails userDetails
	                = userService.loadUserByUsername(authenticationRequest.getUsername());

	        final String token =
	                jwtUtility.generateToken(userDetails);

	        return  new AuthenticationResponse(token);
	    }
	 
	}

 
