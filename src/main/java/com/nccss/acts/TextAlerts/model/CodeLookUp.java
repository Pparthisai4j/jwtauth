package com.nccss.acts.TextAlerts.model;

import java.io.Serializable;

  

public class CodeLookUp implements Serializable
{

	/**
	 * 
	 */
	private static final long	serialVersionUID	= 2130339290208858894L;

	 

	private String				codeId				= "";

	private String				codeDesc			= "";

	/**
	 * @return the codeId
	 */
	public String getCodeId()
	{
		return codeId;
	}

	/**
	 * @param codeId
	 *            the codeId to set
	 */
	public void setCodeId(String codeId)
	{
		this.codeId = codeId;
	}

	/**
	 * @return the codeDetails
	 */
	public String getCodeDesc()
	{
		return codeDesc;
	}

	/**
	 * @param codeDetails
	 *            the codeDetails to set
	 */
	public void setCodeDesc(String codeDetails)
	{
		this.codeDesc = codeDetails;
	}

}
